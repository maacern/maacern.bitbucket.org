var projects = [
    {
        id: 'jscan',
        year: 2008,
        title: 'JScan',
        project: 'JScan (Fi2Net)',
        description: 'Java applet to scan documents',
        technologies: 'jscan.csv',
        challenges: [
            { title: '<span class="highlight">Research</span> : run native code from Java' },
            { title: '<span class="highlight">Testing</span> : memory constraints' },
            { title: '<span class="highlight">Documentation</span> : user manual and technical documents' }
        ]
    },
    {
        id: 'planbook',
        year: 2013,
        title: 'Planbook',
        project: 'Planbook (CERN)',
        description: 'Collaborative planning tool',
        technologies: 'planbook.csv',
        challenges: [
            { title: '<span class="highlight">PL/SQL</span> : speed optimizations'},
            { title: '<span class="highlight">Research</span> : GIT integration' },
            { title: '<span class="highlight">Support</span> : memory issues' },
            { title: '<span class="highlight">Flexibility</span> : Java, Groovy and C#' }
        ]
    },
    {
        id: 'framework',
        year: 2013,
        title: 'AIS20',
        project: 'AIS Framework (CERN)',
        description: 'Framework used internally in the GS-AIS group',
        technologies: 'ais20.csv',
        challenges: [
            { title: '<span class="highlight">Agile</span> : scrum methodology'},
            { title: '<span class="highlight">Teamwork</span> : sprints with 3 persons'},
            { title: '<span class="highlight">Knowledge</span> of GS-AIS business and requirements'},
            { title: '<span class="highlight">Prioritize</span> : select tasks per sprints'},
            { title: '<span class="highlight">Testing/Reviewing</span> : high quality standards'}
        ]
    },
    {
        id: 'women',
        year: 2014,
        title: 'Women\'s Club',
        project: 'Women\'s Club Database (Freelance)',
        description: 'Database administration tool for the CERN Women\'s Club',
        technologies: 'women.csv',
        challenges: [
            { title: '<span class="highlight">Requirements definition</span>'},
            { title: '<span class="highlight">Analyse</span> and <span class="highlight">Design</span> the system'},
            { title: 'User <span class="highlight">training</span>'},
            { title: '<span class="highlight">Maintenance</span> : new user requirements'},
            { title: 'Done in my free time'}
        ]
    },
    {
        id: 'evm4nica',
        year: 2014,
        title: 'EVM4NICA',
        project: 'EVM 4 NICA (CERN)',
        description: 'Tool to provide Earned-Value-Management metrics for JINR',
        technologies: 'evm4nica.csv',
        challenges: [
            { title: '<span class="highlight">Integration</span> with JINR systems'},
            { title: '<span class="highlight">Research</span> : event-driven application'},
            { title: '<span class="highlight">Knowledge sharing</span> : presented to the section'},
            { title: '<span class="highlight">Support</span> : solve issues and questions from JINR'},
            { title: '<span class="highlight">SQL design and tuning</span> : star-schema for reporting'},
            { title: '<span class="highlight">Documentation</span> : technical documentation'}
        ]
    }
];