var timelineData = [
    {
        start: new Date(2002, 10 - 1, 1, 0, 0, 0, 0),
        end: new Date(2005, 9 - 1, 1, 0, 0, 0, 0),
        title: 'Bachelor of Telecommunications Engineering',
        background: '#9585bf',
        index: '1'
    },
    {
        start: new Date(2006, 4 - 1, 1, 0, 0, 0, 0),
        end: new Date(2007, 2 - 1, 1, 0, 0, 0, 0),
        title: 'Software developer at Capgemini',
        background: '#428bca',
        index: '2'
    },
    {
        start: new Date(2007, 3 - 1, 1, 0, 0, 0, 0),
        end: new Date(2012, 11 - 1, 1, 0, 0, 0, 0),
        title: 'Software developer at Fi2net',
        background: '#87b87f',
        index: '3'
    },
    {
        start: new Date(2008, 10 - 1, 1, 0, 0, 0, 0),
        end: new Date(2010, 7 - 1, 1, 0, 0, 0, 0),
        title: 'Master in Web Engineering ',
        background: '#ffb752',
        index: '4'
    },
    {
        start: new Date(2012, 12 - 1, 1, 0, 0, 0, 0),
        end: new Date(),
        title: 'Software developer (PJAS) at CERN',
        background: '#d15b47',
        index: '5'
    }
];