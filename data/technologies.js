var technologies = {
    title: "Languages in my professional career",
    xAxisLabel: "Language",
    yAxisLabel: "Usage in projects",
    data: [
        {name: "Scala", y: 1},
        {name: "C", y: 5},
        {name: "C#", y: 5},
        {name: "Groovy", y: 25},
        {name: "Javascript", y: 40},
        {name: "SQL", y: 75},
        {name: "Java", y: 85}
    ]
};