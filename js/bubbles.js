(function() {
    var root;

    root = typeof exports !== "undefined" && exports !== null ? exports : this;

    this.Bubbles = function(s, f, w, h) {
        var bubbleClass, bubleClasses, chart, clear, click, collide, collisionPadding, connectEvents, data, file, force, gravity, height, idValue, jitter, label, margin, maxRadius, minCollisionRadius, mouseout, mouseover, node, rScale, rValue, selector, textValue, tick, transformData, update, updateLabels, updateNodes, width;
        selector = s;
        file = f;
        width = w;
        height = h;
        data = [];
        node = null;
        label = null;
        margin = {
            top: 5,
            right: 0,
            bottom: 0,
            left: 0
        };
        bubleClasses = {
            1: 'bubble-1',
            2: 'bubble-2',
            3: 'bubble-3',
            4: 'bubble-4',
            5: 'bubble-5',
            6: 'bubble-6',
            7: 'bubble-7',
            8: 'bubble-8',
            9: 'bubble-9',
            10: 'bubble-10'
        };
        maxRadius = 65;
        rScale = d3.scale.sqrt().range([0, maxRadius]);
        rValue = function(d) {
            return parseInt(d.count);
        };
        idValue = function(d) {
            return d.name;
        };
        textValue = function(d) {
            return d.name;
        };
        bubbleClass = function(d) {
            return bubleClasses[d.count];
        };
        collisionPadding = 4;
        minCollisionRadius = 12;
        jitter = 0.5;
        transformData = function(rawData) {
            rawData.forEach(function(d) {
                d.count = parseInt(d.count);
                return rawData.sort(function() {
                    return 0.5 - Math.random();
                });
            });
            return rawData;
        };
        tick = function(e) {
            var dampenedAlpha;
            dampenedAlpha = e.alpha * 0.1;
            node.each(gravity(dampenedAlpha)).each(collide(jitter)).attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
            return label.style("left", function(d) {
                return ((margin.left + d.x) - d.dx / 2) + "px";
            }).style("top", function(d) {
                return ((margin.top + d.y) - d.dy / 2) + "px";
            });
        };
        force = d3.layout.force().gravity(0).charge(0).size([width, height]).on("tick", tick);
        chart = function(selection) {
            return selection.each(function(rawData) {
                var maxDomainValue, svg, svgEnter;
                data = transformData(rawData);
                maxDomainValue = d3.max(data, function(d) {
                    return rValue(d);
                });
                rScale.domain([0, maxDomainValue]);
                svg = d3.select(this).selectAll("svg").data([data]);
                svgEnter = svg.enter().append("svg");
                svg.attr("width", width + margin.left + margin.right);
                svg.attr("height", height + margin.top + margin.bottom);
                node = svgEnter.append("g").attr("id", "bubble-nodes").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                node.append("rect").attr("class", "bubble-background").attr("width", width).attr("height", height).on("click", clear);
                label = d3.select(this).selectAll("#bubble-labels").data([data]).enter().append("div").attr("id", "bubble-labels");
                return update();
            });
        };
        update = function() {
            data.forEach(function(d, i) {
                return d.forceR = Math.max(minCollisionRadius, rScale(rValue(d)));
            });
            force.nodes(data).start();
            updateNodes();
            return updateLabels();
        };
        updateNodes = function() {
            node = node.selectAll(".bubble-node").data(data, function(d) {
                return idValue(d);
            });
            node.exit().remove();
            return node.enter().append("a").attr("class", "bubble-node").attr("xlink:href", function(d) {
                return "#" + (encodeURIComponent(idValue(d)));
            }).call(force.drag).call(connectEvents).append("circle").attr("class", function(d) {
                return bubbleClass(d);
            }).attr("r", function(d) {
                return rScale(rValue(d));
            });
        };
        updateLabels = function() {
            var labelEnter;
            label = label.selectAll(".bubble-label").data(data, function(d) {
                return idValue(d);
            });
            label.exit().remove();
            labelEnter = label.enter().append("a").attr("class", 'bubble-label').attr("href", function(d) {
                return "#href";
            }).call(force.drag).call(connectEvents);
            labelEnter.append("div").attr("class", "bubble-label-name").text(function(d) {
                return textValue(d);
            });
            label.style("font-size", function(d) {
                return Math.max(8, rScale(rValue(d) / 2)) + "px";
            }).style("width", function(d) {
                return 2.5 * rScale(rValue(d)) + "px";
            });
            label.append("span").text(function(d) {
                return textValue(d);
            }).each(function(d) {
                return d.dx = Math.max(2.5 * rScale(rValue(d)), this.getBoundingClientRect().width);
            }).remove();
            label.style("width", function(d) {
                return d.dx + "px";
            });
            return label.each(function(d) {
                return d.dy = this.getBoundingClientRect().height;
            });
        };
        gravity = function(alpha) {
            var ax, ay, cx, cy;
            cx = width / 2;
            cy = height / 2;
            ax = alpha / 8;
            ay = alpha;
            return function(d) {
                d.x += (cx - d.x) * ax;
                return d.y += (cy - d.y) * ay;
            };
        };
        collide = function(jitter) {
            return function(d) {
                return data.forEach(function(d2) {
                    var distance, minDistance, moveX, moveY, x, y;
                    if (d !== d2) {
                        x = d.x - d2.x;
                        y = d.y - d2.y;
                        distance = Math.sqrt(x * x + y * y);
                        minDistance = d.forceR + d2.forceR + collisionPadding;
                        if (distance < minDistance) {
                            distance = (distance - minDistance) / distance * jitter;
                            moveX = x * distance;
                            moveY = y * distance;
                            d.x -= moveX;
                            d.y -= moveY;
                            d2.x += moveX;
                            return d2.y += moveY;
                        }
                    }
                });
            };
        };
        connectEvents = function(d) {
            d.on("click", click);
            d.on("mouseover", mouseover);
            return d.on("mouseout", mouseout);
        };
        clear = function() {
            return location.replace("#");
        };
        click = function(d) {
            return d3.event.preventDefault();
        };
        mouseover = function(d) {
            return node.classed("bubble-hover", function(p) {
                return p === d;
            });
        };
        mouseout = function(d) {
            return node.classed("bubble-hover", false);
        };
        chart.jitter = function(_) {
            if (!arguments.length) {
                return jitter;
            }
            jitter = _;
            force.start();
            return chart;
        };
        chart.height = function(_) {
            if (!arguments.length) {
                return height;
            }
            height = _;
            return chart;
        };
        chart.width = function(_) {
            if (!arguments.length) {
                return width;
            }
            width = _;
            return chart;
        };
        chart.r = function(_) {
            if (!arguments.length) {
                return rValue;
            }
            rValue = _;
            return chart;
        };
        chart.selector = function() {
            return selector;
        };
        chart.file = function() {
            return file;
        };
        chart.plot = function() {
            return d3.csv("data/" + (chart.file()), chart.plotData);
        };
        chart.plotData = function(data) {
            return d3.select(chart.selector()).datum(data).call(chart);
        };
        return chart;
    };

}).call(this);