(function () {

    this.Slides = (function () {

        function Slides() {
            Slides.prototype.months = [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ];

            Slides.prototype.width = 1024;
            Slides.prototype.innerWidth = 1000;
            Slides.prototype.height = 768;
            Slides.prototype.innerHeight = 700;
            Slides.prototype.proportion = Slides.prototype.width / Slides.prototype.height;
            Slides.prototype.left = 50;
        }

        Slides.prototype.init = function () {
            var slides = this;
            $(document).ready(function () {
                initTimelineTemplates(slides);
                initProjectTemplates(slides);
                initPageScroller(slides);
                initAnimations(slides);
                initSlides(slides);
            });
        };

        function initTimelineTemplates(slides) {
            Handlebars.registerHelper('toLocaleDate', function (date) {
                return date.getDate() + " " + slides.months[date.getMonth()] + " " + date.getFullYear();
            });

            var timeline = $('#timeline');
            var yearsTemplate = Handlebars.compile($("#years-template").html());

            var startYear = null;
            var endYear = null;
            var events = [];
            for (var i = 0; i < timelineData.length; i++) {
                var event = timelineData[i];
                var eventStart = event.start.getFullYear();
                var eventEnd = event.end.getFullYear();
                events.push(event);
                if (startYear == null || eventStart < startYear) {
                    startYear = eventStart;
                }
                if (endYear == null || eventEnd > endYear) {
                    endYear = eventEnd;
                }
            }
            var years = [];
            for (var i = startYear; i <= endYear; i++) {
                years.push(i);
            }
            var startDate = new Date(startYear, 0, 1, 0, 0, 0, 0);
            var endDate = new Date(endYear, 11, 31, 0, 0, 0, 0);
            var total = endDate.getTime() - startDate.getTime();
            for (var i = 0 in events) {
                var event = events[i];
                var leftTime = event.start.getTime() - startDate.getTime();
                event.left = (leftTime * 100 / total );
                var widthTime = event.end.getTime() - event.start.getTime();
                event.width = (widthTime * 100 / total);
            }

            var yearHtml = $(yearsTemplate({years: years, events: events}));
            timeline.append(yearHtml);
        }

        function initPageScroller(slides) {
            var labels = [];
            $('section').each(function () {
                var element = $(this);
                labels.push(element.data('section-title'));
            });
            var target = $(document.body);
            var options = {
                scrollOffset: 0,
                keyboardControl: true,
                animationType: 'easeInOutExpo',
                HTML5mode: true,
                animationBefore: function (index) {
                    if (index !== pageScroller.options.currentSection) {
                        $('.slide-current').each(function () {
                            $(this).removeClass('slide-current');
                        });
                    }
                },
                animationComplete: function () {
                    var element = $('.section_' + (pageScroller.options.currentSection + 1));
                    element.addClass("slide-current");

                    element.find('.wow').each(function () {
                        inAnimation(this, '');
                    });

                    element.find('.bubbles').each(function () {
                        initBubbles(slides, $(this));
                    });

                    element.find('.chart').each(function () {
                        initChart(slides, $(this));
                    });

                },
                navigation: labels
            };
            target.pageScroller(options);
            pageScroller.goTo(1);
            $('.slide_1').addClass("slide-current");
            $(document).swipe({
                swipeLeft: function (event, direction, distance, duration, fingerCount) {
                    pageScroller.prev();
                },
                swipeDown: function (event, direction, distance, duration, fingerCount) {
                    pageScroller.next();
                },
                swipeRight: function (event, direction, distance, duration, fingerCount) {
                    pageScroller.next();
                },
                swipeUp: function (event, direction, distance, duration, fingerCount) {
                    pageScroller.prev();
                }
            });
        }

        function initAnimations(slides) {
            var onAnimation = function (box, hidden, duration, delay, iteration, style) {
                if (hidden === true) {
                    outAnimation(box);
                } else {
                    inAnimation(box, style);
                }
            };
            var wow = new WOW({onAnimation: onAnimation, mobile: false});
            wow.init();
        }

        function initSlides(slides) {
            var setViewport = function (index, element) {
                var source = $(window);
                element = $(element);
                element.width(source.width()).height(source.height());

                var slide = element.find('.slide');
                if (slide.length !== 0) {
                    var scaleX = source.width() / slides.width;
                    var newWidth = scaleX * slides.innerWidth;
                    var newHeight = newWidth / slides.proportion;
                    var scaleY = newHeight / slides.height;

                    if (newHeight > source.height()) {
                        scaleY = source.height() / slides.height;
                        newHeight = scaleY * slides.innerHeight;
                        newWidth = newHeight * slides.proportion;
                        scaleX = newWidth / slides.width;
                    }

                    scaleX = Math.round(scaleX * 100) / 100;
                    scaleY = Math.round(scaleY * 100) / 100;
                    var transform = "scale(" + scaleX + "," + scaleY + ")";
                    slide.css({
                        '-ms-transform': transform,
                        '-webkit-transform': transform,
                        'transform': transform
                    });

                    var translateX = Math.round((element.width() - slide.width()) / (2 * scaleX));
                    var translateY = Math.round((element.height() - slide.height()) / (2 * scaleY));
                    transform = transform + " translate(" + translateX + "px," + translateY + "px)";
                    slide.css({
                        '-ms-transform': transform,
                        '-webkit-transform': transform,
                        'transform': transform
                    });
                }
            };
            $('.fullscreen').each(setViewport);

            $('.bubbles').each(function () {
                setDimension($(this));
            });

            $('.chart').each(function () {
                setDimension($(this));
            });

            $('object').each(function () {
                var element = $(this);
                var width = element.width();
                var proportion = parseFloat(element.data('proportion'));
                var height = width / proportion;
                this.addEventListener("load", function () {
                        var svgDoc = element[0].contentDocument;
                        var svg = svgDoc.getElementsByTagName('svg')[0];
                        svg.setAttribute('width', width);
                        svg.setAttribute('height', height);
                        var rect = svgDoc.getElementsByTagName('rect')[0];
                        rect.setAttribute('width', width);
                        rect.setAttribute('height', height);

                        setViewport(0, element.parents('.fullscreen')[0]);
                    },
                    false
                );
            });

            $('.fullscreen').each(setViewport);

            $(window).on('resize', function () {
                $('.fullscreen').each(setViewport)
            });
        }

        function initChart(slides, element) {
            if (element.data('created') == null) {
                element.data('created', true);
                var values = eval(element.data('values'));
                $(element).highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: values.title
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: values.yAxisLabel
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y:.1f}%'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },

                    series: [
                        {
                            name: values.xAxisLabel,
                            colorByPoint: true,
                            data: values.data
                        }
                    ]
                });
            }
        }

        function initBubbles(slides, element) {
            if (element.data('created') == null) {
                element.data('created', true);
                var file = element.data('bubbles');
                new Bubbles('#' + element.attr('id'), file, element.width(), element.height()).plot();
            }
        }

        function initProjectTemplates(slides) {
            var target = $('#projects');
            var projectTemplate = Handlebars.compile($("#project-template").html());
            var sibling = target;
            projects.sort(function (a, b) {
                return a.year - b.year;
            });
            for (var i in projects) {
                var project = projects[i];
                var projectHtml = $(projectTemplate(
                    {id: project.id,
                        year: project.year,
                        project: project.project,
                        description: project.description,
                        technologies: project.technologies,
                        challengesVar: 'projects[' + i + '].challenges',
                        challenges: project.challenges,
                        title: project.title
                    }
                ));
                sibling.after(projectHtml);
                sibling = projectHtml.last();
            }
            target.remove();
        }

        function outAnimation(e) {
            var element = $(e);
            if (!Modernizr.csstransitions && !isMobile(navigator.userAgent)) {
                if (element.hasClass('slideInRight')) {
                    element.css({position: 'relative'});
                    element.animate({ 'left': '2000px' });
                    if (!element.hasClass('animated')) {
                        element.addClass('animated');
                    }
                } else if (element.hasClass('slideInLeft')) {
                    element.css({position: 'relative'});
                    element.animate({ 'left': '-2000px' });
                    if (!element.hasClass('animated')) {
                        element.addClass('animated');
                    }
                }
            }
        }

        function inAnimation(e, style) {
            var element = $(e);
            if (!Modernizr.csstransitions && !isMobile(navigator.userAgent)) {
                if (style === 'slideInRight' || element.hasClass('slideInRight')) {
                    element.animate({ 'left': '0' });
                } else if (style === 'slideInLeft' || element.hasClass('slideInLeft')) {
                    element.animate({ 'left': '0' });
                }
            }
        }

        function setDimension(element) {
            var parent = element.parent();
            var width = parent.width();
            var proportion = parseFloat(parent.data('proportion'));
            element.width(width);
            element.height(width / proportion);
            element.attr('width', width);
            element.attr('height', width / proportion);
        }

        function isMobile(agent) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(agent);
        }

        return Slides;

    })();

}).call(this);

